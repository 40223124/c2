#@+leo-ver=5-thin
#@+node:kmolII_lite.20140426163005.5334: * @file plus.py
#coding: utf8


#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:kmolII_lite.20140426163005.5335: ** <<declarations>> (plus)
import cherrypy
import os
#@-<<declarations>>
#@+others
#@+node:kmolII_lite.20140426163005.5336: ** class plus
#從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
class plus(object):
    """docstring
    從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
    """
    # 表單有中文要加上這個
    _cp_config = {
        # if there is no utf-8 encoding, no Chinese input available
        'tools.encode.encoding': 'utf-8',
    }
    #@+others
    #@+node:kmolII_lite.20140426163005.5337: *3* index
    def index(self):
        '''
        這裡是首頁喔!
        '''
        Outstring = '''
        <h1>從 start 累加到 end, 總數是多少，可指定間隔</h1>
        <h2>假如任意一值為空或不為數字則會導回此頁面,大小填反系統會自動切換</h2>
        <form method="post" action="doAct">
        起始點:<input type=text name=start value=1 ><br />
        結束點:<input type=text name=end value=1 ><br />
        間隔:<input type=text name=sp value=1 ><br />
        <input type="submit" value="計算">
        <input type="reset" value="重置">
        </form>
        '''
        return Outstring
    #@+node:kmolII_lite.20140426163005.5338: *3* doAct
    index.exposed = True

    def doAct(self, start=None, end=None, sp=None):
        total = 0
        if(start or end):
            try:
                start = int(start)
                end = int(end)
                sp = int(sp)
                #轉換失敗 可能是 使用者輸入值不能轉為數字 int 輸入 不為數字 如 : a, a123
            except:
                #轉換失敗 導向首頁
                raise cherrypy.HTTPRedirect("/")
                #return "some input is not correct<br />" + "<a href=\"/index\">Index</a>"
            if start>end:
                start, end = end, start
            for i in range(start, end + 1 , sp):
                total = total + i
            return "總和:" + str(total) +"<br /><a href=\"/\">Index</a>"
        #假如沒有 start or end  導向首頁
        raise cherrypy.HTTPRedirect("/")
    #@-others
    doAct.exposed = True
#@-others
if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(plus(), config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(plus(), config = application_conf)
#@-leo
