#@+leo-ver=5-thin
#@+node:kmolII_lite.20140426163005.5302: * @file input.py
#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:kmolII_lite.20140426163005.5303: ** <<declarations>> (input)
import cherrypy
import os
#@-<<declarations>>
#@+others
#@+node:kmolII_lite.20140426163005.5304: ** class HelloWorld
class HelloWorld(object):
    @cherrypy.expose

    #@+others
    #@+node:kmolII_lite.20140426163005.5305: *3* index
    def index(self):
        檔案 = open("mydata_w.txt", "a", encoding="utf-8")
        檔案.write("123\n")
        檔案.close()
        檔案 = os.remove("mydata_w.txt")
        return  '''<form method='post' action='mysave'> 
學號:<input type='text' name='studno'><br />
姓名:<input type='text' name='studname'><br />
<input type='submit' value='send'></form><br />'''
    #@+node:kmolII_lite.20140426163005.5306: *3* mysave
    @cherrypy.expose
    def mysave(self, studno=None, studname=None):
        檔案 = open("mydata_w.txt", "a", encoding="utf-8")
        檔案.write(studno+","+studname+","+"\n")
        檔案.close()
        檔案 = open("mydata_w.txt", "r", encoding="utf-8")
        數列 = []
        for 行資料 in 檔案:
            數列.append(行資料.strip())
        檔案.close()
        outstring = ""
        for 索引 in range(len(數列)):
            outstring += str(索引)+":"+數列[索引]+"<br />"
        return '''<form method='post' action='mysave'> 
學號:<input type='text' name='studno'><br />
姓名:<input type='text' name='studname'><br />
<input type='submit' value='send'></form><br />'''+outstring+"<br /><br /><a href='/'>回首頁</a>"
    #@-others
#@-others
if __name__ == '__main__':
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(HelloWorld, config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(HelloWorld())
#@-leo
