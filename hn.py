#@+leo-ver=5-thin
#@+node:kmolII_lite.20140426163005.5291: * @file hn.py
#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:kmolII_lite.20140426163005.5292: ** <<declarations>> (hn)
import os
import sys
import cherrypy
import random

# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    sys.path.append(os.path.join(os.getenv("OPENSHIFT_REPO_DIR"), "wsgi"))
else:
    sys.path.append(_curdir)

# 設定在雲端與近端的資料儲存目錄
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 表示程式在雲端執行
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # 表示程式在近端執行
    download_root_dir = _curdir + "/local_data/"
    data_dir = _curdir + "/local_data/"
#@-<<declarations>>
#@+others
#@+node:kmolII_lite.20140426163005.5293: ** class hn
class hn(object):
    # 登入驗證後必須利用 session 機制儲存
    _cp_config = {
    # 配合 utf-8 格式之表單內容
    # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
    'tools.encode.encoding': 'utf-8',
    # 加入 session 設定
    'tools.sessions.on' : True,
    'tools.sessions.storage_type' : 'file',
    'tools.sessions.locking' : 'explicit',
    # 就 OpenShift ./tmp 位於 app-root/runtime/repo/tmp
    # tmp 目錄與 wsgi 目錄同級
    'tools.sessions.storage_path' : data_dir+'tmp',
    # 內定的 session timeout 時間為 60 分鐘
    'tools.sessions.timeout' : 60
    }

#@verbatim
    #@+others
#@verbatim
    #@+node:ppython.20131221094631.1658: *3* index
    #@+others
    #@+node:kmolII_lite.20140426163005.5294: *3* index
    @cherrypy.expose
    def index(self, h=None):
        # 將標準答案存入 answer session 對應區
        超文件輸出 = "<form method=POST action=doCheck>"
        超文件輸出 += "本程式「計算高度h公尺，以g=9.8公尺每秒的重力加速度」的掉落時間<br />請輸入高度<input type=text name=h><br \>"
        超文件輸出 += "<input type=submit value=計算>"
        超文件輸出 += "</form><br /><a href='/'>回首頁</a>"
        return 超文件輸出
    #@+node:kmolII_lite.20140426163005.5295: *3* default
    #@verbatim
    #@+node:ppython.20131221094631.1659: *3* default
    @cherrypy.expose
    def default(self, attr='default'):
        # 內建 default 方法, 找不到執行方法時, 會執行此方法
        return "Page not Found!"
    #@+node:kmolII_lite.20140426163005.5296: *3* doCheck
    #@verbatim
    #@+node:ppython.20131221094631.1660: *3* doCheck
    @cherrypy.expose
    def doCheck(self, h=None):
        g = (9.8)
        h =float(h)
        t= float(2*h/g)
        超文件輸出 = "總共需要"+str(pow(t, 1/2))+"秒(不考慮空氣阻力，且高度遠小於地球半徑的話)<br \>"
        超文件輸出 += "<form method=POST action=doCheck>"
        超文件輸出 += "再輸入一次高度值<input type=text name=h><br \>"
        超文件輸出 += "<input type=submit value=計算>"
        超文件輸出 += "</form><br /><a href='/'>回首頁</a>"
        return 超文件輸出
    #@-others
#@verbatim
    #@-others
#@-others
#@verbatim
#@-others
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"}
    }

if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(hn(), config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''        
        cherrypy.quickstart(hn(), config = application_conf)
#@verbatim
#@-leo
#@-leo
