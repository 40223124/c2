import cherrypy
 
#從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
class plus(object):
#累加程式線上版
    # 表單有中文要加上這個
    _cp_config = {
        # 配合 utf-8 格式之表單內容
        # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
        'tools.encode.encoding': 'utf-8'
    }
    @cherrypy.expose
    def index(self) :
        outstring = '''
        <h1>累加程式線上版</h1>
        <h2>從最小數字依序加到最大數字的程式<br />填入非整數會回到此頁面</h2> 
        <br /><form method="post" action="do">
        起始點:<input type=text name=start value=1 ><br />
        結束點:<input type=text name=end value=1 ><br />
        <input type="submit" value="send">
        <input type="reset" value="reset">
        </form>'''
        return outstring
    @cherrypy.expose
    def do(self, start=None, end=None) :
        sum = 0
        if (start and end):
            try:
                start=int(start) 
                end=int(end)
            except:
                raise cherrypy.HTTPRedirect("/")
                return "輸入錯誤"
        if(start>end):
            start, end =end, start
        for i in range(start, end + 1):
            sum = sum + i
        return "總共:"+str(sum)+"<br /><a href=\"/\">index</a>"
        #假如沒有 start and end  導向首頁
        raise cherrypy.HTTPRedirect("/")


cherrypy.quickstart(plus())


