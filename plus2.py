#@+leo-ver=5-thin
#@+node:kmolII_lite.20140426163005.5324: * @file plus2.py
#coding: utf8


#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:kmolII_lite.20140426163005.5325: ** <<declarations>> (plus2)
import cherrypy
import os
#@-<<declarations>>
#@+others
#@+node:kmolII_lite.20140426163005.5326: ** class plus2
#從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
class plus2(object):
    """docstring
    從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
    """
    # 表單有中文要加上這個
    _cp_config = {
        # if there is no utf-8 encoding, no Chinese input available
        'tools.encode.encoding': 'utf-8',
    }
    #@+others
    #@+node:kmolII_lite.20140426163005.5327: *3* index
    def index(self):
        '''
        這裡是首頁喔!
        '''
        Outstring = '''
        <h1>從 start 累加到 end, 總數是多少(間格為1)，可設定3個避開的數字</h1>
        <h2>假如任意一值為空或不為數字則會導回此頁面,避開的數字可以重複,大小填反系統會自動切換</h2>
        <form method="post" action="doAct">
        起始點:<input type=text name=start value=1 ><br />
        結束點:<input type=text name=end value=1 ><br />
        避開的數字1:<input type=text name=sp1 value=1 ><br />
        避開的數字2:<input type=text name=sp2 value=1 ><br />
        避開的數字3:<input type=text name=sp3 value=1 ><br />
        <input type="submit" value="計算">
        <input type="reset" value="重置">
        </form>
        '''
        return Outstring
    #@+node:kmolII_lite.20140426163005.5328: *3* doAct
    index.exposed = True

    def doAct(self, start=None, end=None, sp1=None, sp2=None, sp3=None):
        total = 0
        on = 0
        on1 = 0
        on2 = 0
        on3 = 0
        if(start or end or sp1 or sp2 or sp3):
            try:
                start = int(start)
                end = int(end)
                sp1 = int(sp1)
                sp2 = int(sp2)
                sp3 = int(sp3)
                #轉換失敗 可能是 使用者輸入值不能轉為數字 int 輸入 不為數字 如 : a, a123
            except:
                #轉換失敗 導向首頁
                raise cherrypy.HTTPRedirect("/")
                #return "some input is not correct<br />" + "<a href=\"/index\">Index</a>"
            if start>end:
                start, end = end, start
            for i in range(start, end + 1):
                #避開要運算的數字，並支援重複輸入
                if i==sp1==sp2==sp3:
                    total = total
                else:
                    if i==sp1==sp2:
                        total = total
                        on1 = 1
                    else:
                        if i==sp1==sp3:
                            total = total
                            on2 = 1
                        else:
                            if i==sp2==sp3:
                                total = total
                                on3 = 1
                            else:
                                total = total + i 
                                on= on+1 
            if on == 7 :
                total = total - sp1 - sp2 - sp3
            if on1 == 1 :
                total = total - sp3
            if on2 == 1 :
                total = total - sp2
            if on3 == 1 :
                total = total - sp1

            return "總和:" + str(total) +"<br /><a href=\"/\">Index</a>"
        #假如沒有 start or end  導向首頁
        raise cherrypy.HTTPRedirect("/")
    #@-others
    doAct.exposed = True
#@-others
if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(plus2(), config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(plus2())
#@-leo
