#@+leo-ver=5-thin
#@+node:kmolII_lite.20140426163005.5313: * @file link.py
#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:kmolII_lite.20140426163005.5314: ** <<declarations>> (link)
import os
import sys
import cherrypy
import random
import math

# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    sys.path.append(os.path.join(os.getenv("OPENSHIFT_REPO_DIR"), "wsgi"))
else:
    sys.path.append(_curdir)

# 設定在雲端與近端的資料儲存目錄
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 表示程式在雲端執行
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # 表示程式在近端執行
    download_root_dir = _curdir + "/local_data/"
    data_dir = _curdir + "/local_data/"
#@-<<declarations>>
#@+others
#@+node:kmolII_lite.20140426163005.5315: ** class link
class link(object):
    # 登入驗證後必須利用 session 機制儲存
    _cp_config = {
    # 配合 utf-8 格式之表單內容
    # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
    'tools.encode.encoding': 'utf-8',
    # 加入 session 設定
    'tools.sessions.on' : True,
    'tools.sessions.storage_type' : 'file',
    'tools.sessions.locking' : 'explicit',
    # 就 OpenShift ./tmp 位於 app-root/runtime/repo/tmp
    # tmp 目錄與 wsgi 目錄同級
    'tools.sessions.storage_path' : data_dir+'tmp',
    # 內定的 session timeout 時間為 60 分鐘
    'tools.sessions.timeout' : 60
    }

#@verbatim
    #@+others
#@verbatim
    #@+node:ppython.20131221094631.1658: *3* index
    #@+others
    #@+node:kmolII_lite.20140426163005.5316: *3* index
    @cherrypy.expose
    def index(self, h=None):
        # 將標準答案存入 answer session 對應區
        超文件輸出 = "<form method=POST action=doCheck>"
        超文件輸出 += "有一根連桿，兩端標示為 C 與 D，且以為圓心旋轉<br />\
此程式可算出使用者指定選轉的任意逆時針角度後的D點座標(X,Y)<br />該座標逆時針旋轉5、15、20、30度的座標(X,Y)<br />和每順時針旋轉0.5度的座標，並以(徑度角, D 點 X 座標, D 點 Y 座標)的格式表示)<br />\
<br />C和D點的座標和旋轉的角度可由使用者自訂<br />請輸入C點座標<input type=text name=CX>,<input type=text name=CY><br />請輸入D點座標<input type=text name=DX>,<input type=text name=DY><br />請輸入逆時針旋轉角度<input type=text name=h><br \>"
        超文件輸出 += "<input type=submit value=計算>"
        超文件輸出 += "</form><br /><a href='/'>回首頁</a>"
        return 超文件輸出
    #@+node:kmolII_lite.20140426163005.5317: *3* default
    #@verbatim
    #@+node:ppython.20131221094631.1659: *3* default
    @cherrypy.expose
    def default(self, attr='default'):
        # 內建 default 方法, 找不到執行方法時, 會執行此方法
        return "Page not Found!"
    #@+node:kmolII_lite.20140426163005.5318: *3* doCheck
    #@verbatim
    #@+node:ppython.20131221094631.1660: *3* doCheck
    @cherrypy.expose
    def doCheck(self, h=None, CX=None, CY=None, DX=None, DY=None):
        '''
        h = float(h)
        X = 150-(50-50*math.cos(math.radians(h)))
        Y = 100+50*math.sin(math.radians(h))
        for i in range(720):
            ra=math.radians(-0.5)
            x=150-(50-50*math.cos((i+1)*ra))
            y=100+50*math.sin((i+1)*ra)
        '''

        CX = float(CX)
        CY = float(CY)
        DX = float(DX)
        DY = float(DY)
        h= float(h)
        R = (pow((CX-DX)*(CX-DX)+(CY-DY)*(CY-DY), 1/2))
        #ran為旋轉角度(徑度)，由於python輸入以徑度量為主，要把弧度轉成徑度
        ran=math.radians(h)
        h0 = math.asin((DY-CY)/R)
        h2=ran + h0
        X = CX+R*math.cos(h2)
        Y = CY+R*math.sin(h2)
        X5 = CX+R*math.cos(math.radians(5)+h0)
        X15 = CX+R*math.cos(math.radians(15)+h0)
        X20 = CX+R*math.cos(math.radians(20)+h0)
        X30 =CX+R*math.cos(math.radians(30)+h0)
        Y5 = CY+R*math.sin(math.radians(5)+h0)
        Y15 = CY+R*math.sin(math.radians(15)+h0)
        Y20 = CY+R*math.sin(math.radians(20)+h0)
        Y30 =CY+R*math.sin(math.radians(30)+h0)
        for i in range(720):
            ra=math.radians(-0.5)
            h1=(i+1)*ra + h0
            x=CX+R*math.cos(h1)
            y=CY+R*math.sin(h1)

            檔案 = open(data_dir+"dcoord.txt", "a", encoding="utf-8")
            檔案.write(str(h1)+","+str(x)+","+str(y)+","+"\n")
            檔案.close()

        超文件輸出 = "您所指定旋轉逆時針轉"+str(h)+"度後的D點座標(X,Y)為：("+str(X)+","+str(Y)+")"+"<br \><br \>"
        超文件輸出 += "逆時針旋轉5度的D點座標(X,Y)為：("+str(X5)+","+str(Y5)+")<br \>"+"逆時針旋轉15度的D點座標(X,Y)為：("+str(X15)+","+str(Y15)+\
")<br \>"+"逆時針旋轉20度的D點座標(X,Y)為：("+str(X20)+","+str(Y20)+")<br \>"+"逆時針旋轉30度的D點座標(X,Y)為：("+str(X30)+","+str(Y30)+")<br \>"
        超文件輸出 += "</form><br /><a href='/'>回首頁</a><br />"
        檔案 = open(data_dir+"dcoord.txt", "r", encoding="utf-8")
        數列 = []
        for 行資料 in 檔案:
            數列.append(行資料.strip())
        檔案.close()
        #刪除檔案
        檔案 = os.remove(data_dir+"dcoord.txt")
        outstring = ""
        for 索引 in range(720):
            outstring += "順時針旋轉"+str((索引+1)*0.5)+"度後D的座標(徑度角,X,Y):<br />"+"("+數列[索引]+")"+"<br />"
        return 超文件輸出+outstring+"<br />"
    #@-others
#@verbatim
    #@-others
#@-others
#@verbatim
#@-others
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"}
    }

if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(link(), config = application_conf)
    else:
        # 近端執行啟動
        
        cherrypy.server.socket_port = 8084
        cherrypy.server.socket_host = '127.0.0.1'
                
        cherrypy.quickstart(link(), config = application_conf)
#@verbatim
#@-leo
#@-leo
